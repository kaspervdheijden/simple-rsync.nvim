## Simple rsync

Quick and dirty rsync plugin for neovim. Also a good project to get to learn lua.

### Installation

[lazy.nvim](https://github.com/folke/lazy.nvim)
```lua
-- lua/plugins/simple-rsync.lua

return {
    remote = 'myname@my.external.machine.hostname:/path/to/remote/project',
--    or:
--    remote = 'my.external.machine.hostname',
--    path   = '/path/to/remote/project',
--    user   = 'myname',
    enabled = function (opts) return opts.remote ~= '' end,
}
```
