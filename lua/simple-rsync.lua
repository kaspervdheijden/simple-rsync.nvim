local config  = nil
local rsyncer = function (action)
    return function (path, opts)
        if config.remote == '' then
            vim.api.nvim_err_writeln('No remote set')
            return
        end

        path = path or vim.fn.expand('%:.')

        local is_dir  = vim.fn.isdirectory(path) == 1
        local timeout = (opts or {}).timeout or (is_dir and config.timeout_dir or config.timeout_file)

        if is_dir and string.sub(path, -1) ~= '/' then
            path = path .. '/'
        end

        local cwd = vim.fn.getcwd()
        if string.sub(path, 1, #cwd) == cwd then
            path = string.sub(path, #cwd + 1)
        end

        while string.sub(path, 1, 1) == '/' do
            path = string.sub(path, 2)
        end

        local cmd
        if action == 'Download' then
            cmd = string.format(
                '%s %s --timeout=%s "%s/%s" "%s" >>%s 2>&1',
                config.rsync_binary,
                config.args,
                timeout,
                config.remote, path,
                path,
                config.logfile
            )
        elseif action == 'Upload' then
            cmd = string.format(
                '%s %s --timeout=%s "%s" "%s/%s" >>%s 2>&1',
                config.rsync_binary,
                config.args,
                timeout,
                path,
                config.remote, path,
                config.logfile
            )
        end

        vim.fn.jobstart(
            cmd,
            {
                on_exit = function (_, exit_code)
                    vim.api.nvim_notify(
                        string.format(exit_code == 0 and '%s of %s complete' or '%s of %s failed with exit code %s', action, path, exit_code),
                        exit_code == 0 and vim.log.levels.INFO or vim.log.levels.ERROR,
                        {}
                    )

                    vim.api.nvim_command('checktime')
                end,
            }
        )
    end
end

return {
    download = rsyncer('Download'),
    upload   = rsyncer('Upload'),
    setup    = function (opts)
        if config ~= nil then
            return
        end

        local o       = opts or {}
        local remote  = o.remote or ''

        if (o.path or '') ~= '' then
            remote = remote .. ':' .. o.path
        end

        if (o.user or '') ~= '' then
            remote = o.user .. '@' .. remote
        end

        config = {
            args         = o.args         or '-zarvCE --no-motd --delete',
            logfile      = o.logfile      or '/dev/null',
            rsync_binary = o.rsync_binary or 'rsync',
            timeout_dir  = o.timeout_dir  or 60,
            timeout_file = o.timeout_file or 4,
            remote       = remote,
        }

        if o.upload_on_save then
            vim.api.nvim_create_autocmd(
                { 'BufWritePost', 'FileWritePost' },
                {
                    group    = vim.api.nvim_create_augroup('rsync-autocmd-upload', { clear = true }),
                    callback = function () rsyncer('Upload')(vim.fn.expand('%:.')) end,
                }
            )
        end

        if o.download_on_read then
            vim.api.nvim_create_autocmd(
                'BufReadPre',
                {
                    group    = vim.api.nvim_create_augroup('rsync-autocmd-download', { clear = true }),
                    callback = function () rsyncer('Download')(vim.fn.expand('%:.')) end,
                }
            )
        end
    end,
}
